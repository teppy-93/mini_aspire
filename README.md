# clone source 
git clone https://thanhloc93@bitbucket.org/teppy-93/mini_aspire.git

cd mini_aspire

cp .env.example .env
# Start docker service 
docker-compose up -d
# get host mysql
docker inspect mysql 

Copy IPAddress and udpate DB_HOST
# exec workspace: 
docker exec -it backend-php /bin/bash
# run migrate
php artisan migrate
# run seed
php artisan db:seed
# remove cache
php artisan cache:clear

php artisan config:cache

php artisan route:cache
# swagger documentation
http://localhost:8080/api/documentation
# register user
http://localhost:8080/api/auth/register
# login user
http://localhost:8080/api/auth/login
=> token
# require tokens: set header Authorization: Bearer token
# user info
http://localhost:8080/api/me
# get list loan of user
http://localhost:8080/api/me/loans
# get list user
http://localhost:8080/api/users
# get list loans
http://localhost:8080/api/loans

# list repayment of loan
http://localhost:8080/api/loan/{id}/repayment
# post loan: state = pending
http://localhost:8080/api/loan

# PUT: active loan: state from pending => active
http://localhost:8080/api/loan/{id}/active

# api for crontab payment: 0 0 1 * *
# POST: setup run every month day 1
http://localhost:8080/api/payment

# POST prepay: prepayment: 2% of the total amount for the first 2 years. 1% 3rd year. 4th year onwards free. Config in table interest_configs
http://localhost:8080/api/prepay
# GET: calculator prepay
http://localhost:8080/api/prepay/calculator

# DELETE: cancel loan: state from pending => cancel
http://localhost:8080/api/loan/{id}/cancel

# run test
php artisan test