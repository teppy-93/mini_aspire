<?php

namespace Tests\Feature\Loan;
use JWTAuth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoanControllerTest extends TestCase
{
    protected function headers($user = null)
    {
        $headers = ['Accept' => 'application/json'];

        if (!is_null($user)) {
            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);
            $headers['Authorization'] = 'Bearer '.$token;
        }

        return $headers;
    }
    public function testCreateLoanAndPrepay()
    {
        // create loan
        $data = [
            "arrangement_fee"=>1000000,
            "interest_rate"=>10,
            "amount"=>1000000000,
            "duration"=>240,
            "repayment_frequency"=>1
        ];
        $headers =   $this->headers(\App\Models\User::whereEmail('aspire@gmail.com')->first());
        $response =$this->withHeaders($headers)->post('api/loan', $data)
            ->assertStatus(200);
        $loan = json_decode($response->getContent());
        $loan_id = $loan->data->id;
        echo 'create loan ok id ='. $loan_id;

        // active loan
        $response =$this->withHeaders($headers)->put('api/loan/'.$loan_id.'/active')
            ->assertStatus(200);
        $active = json_decode($response->getContent());
        echo 'active loand ok';

        // prepayloan
        $response =$this->withHeaders($headers)->post('api/prepay', [
            'amount'=>10000000,
            'loan_id' => $loan_id
        ])
        ->assertStatus(200);
        $prepay = json_decode($response->getContent());
        var_dump($prepay->data->total);
    }


    public function testCreateLoanAndPayment()
    {
        // create loan
        $data = [
            "arrangement_fee"=>1000000,
            "interest_rate"=>10,
            "amount"=>1000000000,
            "duration"=>240,
            "repayment_frequency"=>1
        ];
        $headers =   $this->headers(\App\Models\User::whereEmail('aspire@gmail.com')->first());
        $response =$this->withHeaders($headers)->post('api/loan', $data)
            ->assertStatus(200);
        $loan = json_decode($response->getContent());
        $loan_id = $loan->data->id;
        echo 'create loan ok id ='. $loan_id;

        // active loan
        $response =$this->withHeaders($headers)->put('api/loan/'.$loan_id.'/active')
            ->assertStatus(200);
        $active = json_decode($response->getContent());
        echo 'active loand ok';

        // prepayloan
        $response =$this->withHeaders($headers)->post('api/payment')
        ->assertStatus(200);
        $prepay = json_decode($response->getContent());
        var_dump($prepay);
    }
}