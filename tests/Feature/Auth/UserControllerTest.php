<?php

namespace Tests\Feature\Auth;
use JWTAuth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserControllerTest extends TestCase {
    protected function headers($user = null)
    {
        $headers = ['Accept' => 'application/json'];

        if (!is_null($user)) {
            $token = JWTAuth::fromUser($user);
            JWTAuth::setToken($token);
            $headers['Authorization'] = 'Bearer '.$token;
        }

        return $headers;
    }
    public function testRequireEmailAndLogin(){
        $this->json('POST', 'api/auth/login')
            ->assertStatus(400)                
            ->assertJson([
                "error" => [
                    "email" => [
                        "Email is required"
                    ],
                    "password" => [
                        "Password is required"
                    ]
                ]
            ]);          
    }


    public function testUserLoginSuccessfully(){
        $user = ['email' => 'aspire@gmail.com', 'password' => '123456'];
        $this->json('POST', 'api/auth/login', $user)
            ->assertStatus(200)
            ->assertJsonStructure([
                'token' ]);
    }

    public function testListUserSuccessfully(){
        $headers =   $this->headers(\App\Models\User::whereEmail('aspire@gmail.com')->first());
        $list = $this->json('GET', 'api/users?page=1&page_size=10', $headers)
            ->assertStatus(200);
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function testRegisterSuccessfully()
    {
        $rand = $this->generateRandomString();
        $register = [
            "email"=>"locnguyen". $rand ."@gmail.com",
            "name"=>"thành",
            "password"=>123456,
            "phone"=>$rand ,
            "address"=>"thành",
            "card_id"=>"0323213222" 
        ];

        $this->json('POST', 'api/auth/register', $register)
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'message',
                'data' => [
                    'email',
                    'name',
                    'phone',
                    'password',
                    'address',
                    'card_id',
                    'id',
                    'created_at',
                    'updated_at'
                ]                
            ]);
    }
    public function testUserInfo(){
        $token = auth()->guard('api')
        ->login(\App\Models\User::whereEmail('aspire@gmail.com')->first());
        $this->withHeader('Authorization', 'Bearer ' . $token)->json('GET', 'api/me')
            ->assertStatus(200);
    }


    public function testListLoanUserSuccessfully(){
        $headers =   $this->headers(\App\Models\User::whereEmail('aspire@gmail.com')->first());
        $this->json('GET', 'api/me/loans?page=1&page_size=10', $headers )
            ->assertStatus(200);
    }
}


