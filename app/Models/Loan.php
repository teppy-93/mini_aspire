<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;
use App\Models\Repayment;
class Loan extends Eloquent
{
    protected $table = 'loans';
    protected $guarded = ['id']; 
    protected static function boot()
    {
        parent::boot();
    }

    public function repayments(){
        return $this->hasMany('App\Models\Repayment', 'loan_id');
    }
   
}




