<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;
class Repayment extends Eloquent
{
    protected $table = 'repayments';
    protected $guarded = ['id']; 
    protected static function boot()
    {
        parent::boot();
    }
}




