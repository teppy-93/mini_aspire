<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;
class InterestConfig extends Eloquent
{
    protected $table = 'interest_configs';
    protected $guarded = ['id']; 
    protected static function boot()
    {
        parent::boot();
    }
}




