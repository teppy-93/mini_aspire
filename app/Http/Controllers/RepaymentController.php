<?php
namespace App\Http\Controllers;
use App\Models\Loan;
use App\Models\Repayment;
use App\Models\InterestConfig;
use Illuminate\Validation\Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Illuminate\Support\Facades\Auth;

class RepaymentController extends Controller
{
    private $user;
    public function __construct()
    {
        parent::__construct();
        $this->user = Auth::user();
    }
    /**
     * @OA\Post(
     *      path="/api/prepay/calculator",
     *      tags={"Repayment"},
     *      summary="tính tiền phạt khi trả trước hạn",
     *      description="tính tiền phạt khi trả trước hạn",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Prepay")
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    public function calculator(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                "amount" => "required|gte:100000",
                "loan_id" => "required",
            ],
            [
                "amount.required" => "Amount is required",
                "amount.gte" => "Amount min is 100k",
                "loan_id.required" => "Loan is required",
            ]
        );
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $param = (object) $request->only("amount", "loan_id");
        $loan = Loan::where("id", $param->loan_id)
            ->where("state", "active")
            ->where("user_id", $this->user->id)
            ->first();
        if (empty($loan)) {
            return response()->json(
                ["error" => false, "error" => "Loan not found"],
                400
            );
        }
        if ($loan->amount - $loan->original_paid <= 0) {
            return response()->json(
                ["error" => false, "error" => "Loan has been paid"],
                400
            );
        }

        if (Carbon::parse($loan->end_date)->timestamp < strtotime("now")) {
            return response()->json(
                ["error" => false, "error" => "Loan is past due"],
                400
            );
        }

        if ($loan->amount - $loan->original_paid < $param->amount) {
            return response()->json(
                [
                    "error" => false,
                    "error" => "Amount cannot be greater than the loan",
                ],
                400
            );
        }
        $diffMonth = Carbon::now()->diffInMonths($loan->disbursement_date);
        $interestConfig = InterestConfig::where("from_month", "<=", $diffMonth)
            ->where("to_month", ">=", $diffMonth)
            ->first();
        $interest = !empty($interestConfig->interest)
            ? $interestConfig->interest
            : 0;
        $interest_amount = ($param->amount / 100) * $interest;
        return response()->json([
            "status" => 200,
            "message" => "Check successfully",
            "data" => [
                "total" => $param->amount + $interest_amount,
                "remaining_debt" => $loan->amount - $loan->original_paid,
                "original_paid" => $param->amount,
                "prepayment_penalty" => $interest . "%",
                "interest" => $interest_amount,
                "end_date" => $loan->end_date,
                "state" => $loan->state,
            ],
        ]);
    }
    /**
     * @OA\Post(
     *      path="/api/prepay",
     *      tags={"Repayment"},
     *      summary="thanh toán trước hạn",
     *      description="thanh toán trước hạn. 1=>2 năm đầu: lãi 2%. năm thứ 3: 1%. thứ 4 trở đi miễn phí",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Prepay")
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    /**
     * @OA\Schema(
     *   schema="Prepay",
     *   @OA\Property(
     *     property="amount",
     *     type="number"
     *   ),
     *   @OA\Property(
     *     property="loan_id",
     *     type="number"
     *   )
     * )
     *
     */
    public function prepay(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                "amount" => "required|gte:100000",
                "loan_id" => "required",
            ],
            [
                "amount.required" => "Amount is required",
                "amount.gte" => "Amount min is 100k",
                "loan_id.required" => "Loan is required",
            ]
        );
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $param = (object) $request->only("amount", "loan_id");
        $loan = Loan::where("id", $param->loan_id)
            ->where("state", "active")
            ->where("user_id", $this->user->id)
            ->first();
        if (empty($loan)) {
            return response()->json(
                ["error" => false, "error" => "Loan not found"],
                400
            );
        }
        if ($loan->amount - $loan->original_paid <= 0) {
            return response()->json(
                ["error" => false, "error" => "Loan has been paid"],
                400
            );
        }
        // quá hạn thanh toán
        if (Carbon::parse($loan->end_date)->timestamp < strtotime("now")) {
            return response()->json(
                ["error" => false, "error" => "Loan is past due. Contact us"],
                400
            );
        }
        // số tiền trả ko dc lớn hơn số tiền còn lại đang nợ
        if ($loan->amount - $loan->original_paid < $param->amount) {
            return response()->json(
                [
                    "error" => false,
                    "error" => "Amount cannot be greater than the loan",
                ],
                400
            );
        }
        // Lấy % phạt nếu trả trước hạn
        $diffMonth = Carbon::now()->diffInMonths($loan->disbursement_date);
        $interestConfig = InterestConfig::where("from_month", "<=", $diffMonth)
            ->where("to_month", ">=", $diffMonth)
            ->first();
        $interest = !empty($interestConfig->interest)
            ? $interestConfig->interest
            : 0;
        // Số tiền bị phạt
        $interest_amount = ($param->amount / 100) * $interest;
        DB::beginTransaction();
        try {
            $repayment = Repayment::create([
                "user_id" => $this->user->id,
                "loan_id" => $param->loan_id,
                "amount" => $param->amount,
                "interest" => $interest_amount,
                "original" => $param->amount,
                "type" => "prepay",
                "note" => "Trả trước hạn và bị phạt $interest% trên tổng tiền trả",
            ]);
            /// udpate gốc trong bảng loan
            $loan->original_paid += $param->amount;
            $loan->state =
                $loan->original_paid == $loan->amount ? "paid" : "active";
            $loan->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "status" => 400,
                "message" => "Loan created fail",
                "data" => $loan,
            ]);
        }
        $total = $interest_amount + $param->amount;
        return response()->json([
            "status" => 200,
            "message" => "Loan prepay successfully",
            "data" => [
                "total" => "Tổng tiền phải trả là {$total}",
                "loan" => $loan,
                "repayment" => $repayment,
            ],
        ]);
    }

    /**
     * @OA\Post(
     *      path="/api/payment",
     *      tags={"Repayment"},
     *      summary="API cắt tiền khi tới hạn",
     *      description="API cắt tiền khi tới hạn",
     *     @OA\Response(response="default", description="")
     * )
     */
    // api crontab chạy vào ngày 1 hàng tháng
    public function payment(Request $request)
    {
        // mặc định user có sẵn tiền để thanh toán
        $loans = Loan::where("state", "active")
            ->where("end_date", ">=", Carbon::now()->format("Y-m-d H:i:s"))
            ->get();
        foreach ($loans as $loan) {
            // get newest payment
            $newestPayment = Repayment::where("loan_id", $loan->id)
                ->where("type", "auto")
                ->orderBy("created_at", "desc")
                ->first();
            $date = !empty($newestPayment)
                ? $newestPayment->created_at
                : $loan->disbursement_date;
            $diffMonth = Carbon::now()->diffInMonths($date) + 1;
            $diffDate = Carbon::now()->diffInDays($date);

            if ($diffMonth == $loan->repayment_frequency) {
                $original_paid = $loan->amount / $loan->duration;
                $interest =
                    ($loan->amount - $loan->original_paid) *
                    ($loan->interest_rate / 365 / 100) *
                    $diffDate;
                DB::beginTransaction();
                try {
                    $repayment = Repayment::create([
                        "user_id" => $loan->user_id,
                        "loan_id" => $loan->id,
                        "amount" => $original_paid + $interest,
                        "interest" => $interest,
                        "original" => $original_paid,
                        "type" => "auto",
                        "note" => "Tự động cắt",
                    ]);
                    /// udpate gốc trong bảng loan
                    $loan->original_paid += $original_paid;
                    $loan->state =
                        $loan->original_paid == $loan->amount
                            ? "paid"
                            : "active";
                    $loan->save();
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    return response()->json([
                        "status" => 400,
                        "message" => "Loan created fail",
                        "data" => $loan,
                    ]);
                }
            }
        }
        return response()->json([
            "status" => 200,
            "message" => "Payment successfully",
        ]);
    }
}
