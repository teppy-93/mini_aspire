<?php
namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Validation\Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;
use JWTAuthException;
use Hash;

class UserController extends Controller
{
    private $user;
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }
    /**
     * @OA\Get(
     *      path="/api/users",
     * tags={"User"},
     *     description="Returns list of user",
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *    @OA\Parameter(
     *          name="page_size",
     *          description="page_size",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(response="default", description=""
 *     ))
     * )
     */

    public function index(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "page" => ["required", "integer"],
            "page_size" => ["required", "integer"],
        ]);
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $page = $request->page - 1;
        $page_size = $request->page_size;
        if ($page) {
            $page = $page_size * $page;
        }
        $result = User::skip($page)
            ->take($page_size)
            ->orderBy("id", "desc")
            ->get();
        return response()->json(["success" => true, "data" => $result], 200);
    }
    /**
     * @OA\Post(
     *      path="/api/auth/register",
     *      tags={"User"},
     *      summary="add user",
     *      description="add user",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    /**
     * @OA\Schema(
     *   schema="User",
     *   @OA\Property(
     *     property="email",
     *     type="string"
     *   ),
     *   @OA\Property(
     *     property="name",
     *     type="string"
     *   ),
     *   @OA\Property(
     *     property="phone",
     *     type="string"
     *   ),
     *   *   @OA\Property(
     *     property="card_id",
     *     type="string"
     *   ),
     *     @OA\Property(
     *     property="password",
     *     type="string"
     *   )
     * )
     *
     */
    public function register(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                "email" => "required|email|max:200|unique:users,email",
                "phone" => "required|max:200|unique:users,phone",
                "name" => "required|max:50",
                "card_id" => "required|max:15",
                "password" => "required|max:20|min:6",
            ],
            [
                "email.required" => "Email is required",
                "email.max" => "Email: max 50 character",
                "email.unique" => "Email is unique",
                "phone.required" => "Phone is required",
                "phone.max" => "Phone: max 50 character",
                "phone.unique" => "Phone is unique",
                "password.required" => "Password is required",
                "password.max" => "Password: 6-20 character",
                "password.min" => "Password: 6-20 character",
                "card_id.required" => "CMND is required",
                "name.required" => "Name is required",
                "card_id.max" => "CMND: max 15 character",
            ]
        );
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $param = (array) $request->only(
            "email",
            "name",
            "phone",
            "password",
            "address",
            "card_id"
        );
        $param["password"] = Hash::make($request->get("password"));
        $user = $this->user->create($param);

        return response()->json([
            "status" => 200,
            "message" => "User created successfully",
            "data" => $user,
        ]);
    }
    /**
     * @OA\Post(
     *      path="/api/auth/login",
     *      tags={"User"},
     *      summary="user login",
     *      description="user login",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UserLogin")
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    /**
     * @OA\Schema(
     *   schema="UserLogin",
     *   @OA\Property(
     *     property="email",
     *     type="string"
     *   ),
     *     @OA\Property(
     *     property="password",
     *     type="string"
     *   )
     * )
     *
     */
    public function login(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                "email" => "required",
                "password" => "required|max:20|min:6",
            ],
            [
                "email.required" => "Email is required",
                "password.required" => "Password is required",
                "password.max" => "Password: 6-20 character",
                "password.min" => "Password: 6-20 character",
            ]
        );
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $credentials = $request->only("email", "password");
        $token = null;
        try {
            if (!($token = JWTAuth::attempt($credentials))) {
                return response()->json(["invalid_email_or_password"], 422);
            }
        } catch (JWTAuthException $e) {
            return response()->json(["failed_to_create_token"], 500);
        }
        return response()->json(compact("token"));
    }

    /**
     * @OA\Get(
     *      path="/api/me",
     * tags={"User"},
     *     description="Return user info",
     *     @OA\Response(response="default", description="")
     * )
     */
    public function getUserInfo(Request $request)
    {
        $user = JWTAuth::toUser($request->token);
        return response()->json(["data" => $user]);
    }
    /**
     * @OA\Get(
     *      path="/api/me/loans",
     * tags={"User"},
     *     description="Returns list loans of user",
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *    @OA\Parameter(
     *          name="page_size",
     *          description="page_size",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    public function loans(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "page" => ["required", "integer"],
            "page_size" => ["required", "integer"],
        ]);
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $page = $request->page - 1;
        $page_size = $request->page_size;
        $user = JWTAuth::toUser($request->token);
        $loans = User::where("id", $user->id)
            ->first()
            ->loans()
            ->paginate($page_size);
        return response()->json(["success" => true, "data" => $loans], 200);
    }
}
