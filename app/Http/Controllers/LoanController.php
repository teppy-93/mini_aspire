<?php
namespace App\Http\Controllers;
use App\Models\Loan;
use Illuminate\Validation\Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Illuminate\Support\Facades\Auth;

class LoanController extends Controller
{
    private $user;
    public function __construct()
    {
        parent::__construct();
        $this->user = Auth::user();
    }
    /**
     * @OA\Get(
     *      path="/api/loans",
     * tags={"Loans"},
     *     description="Returns list loans",
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *    @OA\Parameter(
     *          name="page_size",
     *          description="page_size",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    public function index(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            "page" => ["required", "integer"],
            "page_size" => ["required", "integer"],
        ]);
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $page = $request->page - 1;
        $page_size = $request->page_size;
        if ($page) {
            $page = $page_size * $page;
        }
        $result = Loan::where("user_id", $this->user->id)
            ->whereIn("state", ["active", "paid"])
            ->skip($page)
            ->take($page_size)
            ->orderBy("id", "desc")
            ->get([
                "id",
                "amount",
                "original_paid",
                "duration",
                "repayment_frequency",
                "state",
                "interest_rate",
                "arrangement_fee",
                "arrangement_state",
                "disbursement_date",
                "end_date",
                DB::raw("(amount - original_paid) as rest"),
            ]);
        return response()->json(["success" => true, "data" => $result], 200);
    }
    /**
     * @OA\Get(
     *      path="/loan/{id}/repayment",
     * tags={"Loans"},
     *     description="Returns repayment of loan",
     *      @OA\Parameter(
     *          name="page",
     *          description="page",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *    @OA\Parameter(
     *          name="page_size",
     *          description="page_size",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    public function repayment(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            "page" => ["required", "integer"],
            "page_size" => ["required", "integer"],
        ]);
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $page = $request->page - 1;
        $page_size = $request->page_size;
        $repayment = Loan::where("user_id", $this->user->id)
            ->where("id", $id)
            ->first()
            ->repayments()
            ->paginate($page_size);
        return response()->json(["success" => true, "data" => $repayment], 200);
    }
    /**
     * @OA\Post(
     *      path="/api/loan",
     *      tags={"Loans"},
     *      summary="add loan",
     *      description="add loan",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Loan")
     *      ),
     *     @OA\Response(response="default", description="")
     * )
     */
    /**
     * @OA\Schema(
     *   schema="Loan",
     *   @OA\Property(
     *     property="amount",
     *  description="số tiền vay",
     *     type="number"
     *   ),
     *   *   @OA\Property(
     *     property="duration",
     * *     description="tháng",
     *     type="number"
     *   ),
     *     @OA\Property(
     *     property="repayment_frequency",
     *     type="number"
     *   ),
     *  *  *     @OA\Property(
     *     property="interest_rate",
     *     type="number"
     *   ),
     *  @OA\Property(
     *     property="arrangement_fee",
     *     type="number"
     *   ),
     * )
     *
     */
    public function store(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                "arrangement_fee" => "required|gte:0",
                "interest_rate" => "required|gte:0",
                "amount" => "required|gte:1000000",
                "duration" => "required|gte:1",
                "repayment_frequency" => "required|in:1,3,6",
            ],
            [
                "amount.required" => "Amount is required",
                "amount.gte" => "Amount min is 1tr",
                "duration.required" => "Duration is required",
                "duration.gte" => "Duration min is 1 month",
                "repayment_frequency.required" =>
                    "Repayment frequency is required",
                "repayment_frequency.in" => "Repayment frequency is: 1,3,6",
                "interest_rate.required" => "Interest rate is required",
                "arrangement_fee.required" => "Arrangement fee is required",
            ]
        );
        if ($validator->fails()) {
            return response()->json(
                ["error" => false, "error" => $validator->errors()],
                400
            );
        }
        $param = (array) $request->only(
            "amount",
            "duration",
            "interest_rate",
            "arrangement_fee",
            "repayment_frequency"
        );
        $param = array_merge(
            [
                "user_id" => $this->user->id,
                "original_paid" => 0,
                "state" => "pending",
            ],
            $param
        );
        $loan = Loan::create($param);
        return response()->json([
            "status" => 200,
            "message" => "Loan created successfully",
            "data" => $loan,
        ]);
    }
    /**
     * @OA\Put(
     *      path="/loan/{id}/active",
     *      tags={"Loans"},
     *      summary="active loan",
     *      description="active loan",
     *     @OA\Response(response="default", description="")
     * )
     */
    public function active(Request $request, $id)
    {
        $loan = Loan::where("id", $id)
            ->where("state", "pending")
            ->where("user_id", $this->user->id)
            ->first();
        if (empty($loan)) {
            return response()->json(
                ["error" => false, "error" => "Loan not found"],
                400
            );
        }
        $loan->state = "active";
        $loan->disbursement_date = Carbon::now();
        $loan->end_date = Carbon::now()
            ->add($loan->duration, "months")
            ->format("Y-m-d H:i:s");
        if ($loan->save()) {
            return response()->json([
                "status" => 200,
                "message" => "Loan active successfully",
                "data" => $loan,
            ]);
        } else {
            return response()->json([
                "status" => 400,
                "message" => "Loan active fail",
                "data" => $loan,
            ]);
        }
    }
    /**
     * @OA\Delete(
     *      path="/loan/{id}/cancel",
     *      tags={"Loans"},
     *      summary="delete loan",
     *      description="delete loan",
     *     @OA\Response(response="default", description="")
     * )
     */
    public function cancel(Request $request, $id)
    {
        $loan = Loan::where("id", $id)
            ->where("state", "pending")
            ->where("user_id", $this->user->id)
            ->first();
        if (empty($loan)) {
            return response()->json(
                ["error" => false, "error" => "Loan not found"],
                400
            );
        }
        $loan->state = "cancel";
        if ($loan->save()) {
            return response()->json([
                "status" => 200,
                "message" => "Loan cancel successfully",
                "data" => $loan,
            ]);
        } else {
            return response()->json([
                "status" => 400,
                "message" => "Loan cancel fail",
                "data" => $loan,
            ]);
        }
    }
}
