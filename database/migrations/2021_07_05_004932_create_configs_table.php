<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_configs', function (Blueprint $table) {
            $table->id();
            $table->integer('from_month')->description('từ tháng');
            $table->integer('to_month')->description('đến tháng');
            $table->integer('interest')->description('lãi phạt khi trả trước hạn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_configs');
    }
}
