<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->integer('amount')->description('số tiền vay');
            $table->integer('original_paid')->description('gốc còn lại');
            $table->integer('duration')->description('thời hạn vay (tháng)');
            $table->integer('repayment_frequency')->description('tần suất trả nợ (tháng)');
            $table->enum('state', ['pending', 'active', 'paid', 'cancel'])->default('pending');
            $table->integer('interest_rate')->default(0);
            $table->integer('arrangement_fee')->default(0);
            $table->datetime('disbursement_date')->description('ngày giải ngân')->nullable();
            $table->datetime('end_date')->description('ngày kết thúc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
