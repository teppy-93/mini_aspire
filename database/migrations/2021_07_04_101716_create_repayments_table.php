<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayments', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->description('người dùng');
            $table->integer('loan_id')->description('khoản vay');
            $table->integer('amount')->description('số tiền trả');
            $table->integer('interest')->description('lãi');
            $table->integer('original')->description('gốc');
            $table->enum('type', ['auto', 'prepay'])->description('tự động cắt hay trả trước');
            $table->text('note')->description('ghi chú')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repayments');
    }
}
