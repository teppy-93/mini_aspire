<?php

namespace Database\Seeders;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Lộc Thành',
            'email' => 'aspire@gmail.com',
            'password' => bcrypt('123456'),
            'phone' =>  '0838888388',
            'address' =>  'HCM',
            'card_id' =>  '3819999999',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        DB::table('loans')->insert([
            'user_id'=>1,
            'amount' => 100000000,
            'original_paid' => 0,
            'duration' => 120,
            'repayment_frequency' =>1,
            'state' =>  'pending',
            'interest_rate' => 10,
            'arrangement_fee' => 1000000,
            'disbursement_date' => '2021-05-01 08:00:00',
            'end_date' => '2031-05-09 08:00:00',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
        /*
        Lãi phạt trả trước hạn:
              1 -> 24 tháng: 2% / số tiền trả
              25 -> 36 tháng: 1% / số tiền trả
              37 tháng trở đi: miễn phí
        */
        DB::table('interest_configs')->insert([[
            'from_month'=>0,
            'to_month' => 24,
            'interest' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'from_month'=>25,
            'to_month' => 36,
            'interest' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'from_month'=>37,
            'to_month' => 9999,
            'interest' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]]);
    }
}
