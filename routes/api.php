<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoanController;
use App\Http\Controllers\RepaymentController;

Route::post('auth/register', [UserController::class, 'register']);
Route::post('auth/login',  [UserController::class, 'login']);
Route::group(['middleware' => 'jwt.auth'], function () {

    Route::get('me', [UserController::class, 'getUserInfo']);
    Route::get('me/loans', [UserController::class, 'loans']);
    Route::get('users', [UserController::class, 'index']);

    Route::get('loans', [LoanController::class, 'index']);
    Route::post('loan', [LoanController::class, 'store']);
    Route::get('loan/{id}/repayment', [LoanController::class, 'repayment']);
    Route::put('loan/{id}/active', [LoanController::class, 'active']);
    Route::delete('loan/{id}/cancel', [LoanController::class, 'cancel']);

    Route::post('payment', [RepaymentController::class, 'payment']);
    Route::post('prepay', [RepaymentController::class, 'prepay']);
    Route::post('prepay/calculator', [RepaymentController::class, 'calculator']);
});